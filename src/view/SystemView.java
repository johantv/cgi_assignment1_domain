/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

/**
 *
 * @author johantv
 */
public class SystemView {
    public void printInformation(int id, String name, String phone, String addr, String bDay, String nat, String lang, String soc) {
        System.out.println("* * * * * * * * *");
        System.out.println(
            "id: " + id + "\n" +
            "Name: " + name + "\n" +
            "Phone: " + phone + "\n" +
            "Address: " + addr + "\n" +
            "Date of birth: " + bDay + "\n" +
            "Nationality: " + nat + "\n" +
            "Language: " + lang + "\n" +
            "Social security number: " + soc + "\n");
    }
    public void printAccess(boolean b) {
        System.out.println("User has access? " + b);
    }
}
