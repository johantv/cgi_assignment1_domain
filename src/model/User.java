/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author johantv
 */
public class User {
    //this class is for getting users username and password from gui and db
    private final String userNameDatabase = "exampleun";
    private final String passwordDatabase = "examplepw";
    private String userName, password;
    
    
    public User(String un, String pw) {
        this.userName = un;
        this.password = pw;
    }
    
    public String getUsernameDB() {
        return this.userNameDatabase;
    }
    
    public String getPasswordDB() {
        return this.passwordDatabase;
    }
    
    public String getUsername() {
        return userName;
    }
    public String getPassword() {
        return password;
    }
}
