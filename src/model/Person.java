/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author johantv
 */
public class Person {
    //this class is for passing information between user interface and database
    //personal information
    private int id;
    private String name, phone, address, birthDay, nationality, language, socialSecurityNumber;
    
    public Person(int id, String name, String phone, String addr, String bDay, String nat, String lang, String soc) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.address = addr;
        this.birthDay = bDay;
        this.nationality = nat;
        this.language = lang;
        this.socialSecurityNumber = soc;
    }
    
    //method for updating person information
    public void modifyPerson(String fieldToModify, String newValue) {
        switch(fieldToModify.toLowerCase()) {
            case "name" : 
                setName(newValue);
                break;
            case "phone" : 
                setPhone(newValue);
                break;
            case "address" : 
                setAddress(newValue);
                break;
            case "birthday" : 
                setBirthDay(newValue);
                break;
            case "nationality" : 
                setNationality(newValue);
                break;
            case "language" : 
                setLanguage(newValue);
                break;
            case "social security number" : 
                setSocialSecurityNumber(newValue);
                break;
            
        }
        
    }
    //getters 
    public int getId() {
        return this.id;
    }
    public String getName() {
        return this.name;
    }    
    public String getPhone() {
        return this.phone;
    }    
    public String getAddress() {
        return this.address;
    }    
    public String getBirthDay() {
        return this.birthDay;
    }    
    public String getNationality() {
        return this.nationality;
    }    
    public String getLanguage() {
        return this.language;
    }    
    public String getSocialSecurityNumber() {
        return this.socialSecurityNumber;
    }
    
    //setters
    public void setName(String name) {
        this.name = name;
    }    
    public void setPhone(String phone) {
        this.phone = phone;
    }    
    public void setAddress(String addr) {
        this.address = addr;
    }    
    public void setBirthDay(String bDay) {
        this.birthDay = bDay;
    }    
    public void setNationality(String nat) {
        this.nationality = nat;
    }    
    public void setLanguage(String lang) {
        this.language = lang;
    }    
    public void setSocialSecurityNumber(String soc) {
        this.socialSecurityNumber = soc;
    }   
    
}
