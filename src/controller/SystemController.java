/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Person;
import model.User;
import view.SystemView;

/**
 *
 * @author johantv
 */
public class SystemController {
    private User user;
    private Person model;
    private SystemView view;
    
    //constructor for person
    public SystemController(Person model, SystemView view) {
        this.model = model;
        this.view = view;
        this.user = user;
    }
    
    //constructor for user
    public SystemController(User userModel, SystemView view) {
        this.user = userModel;
        this.view = view;
    }
    
    //getters: Person
    public int getPersonId() {
        return model.getId();
    }
    public String getPersonName() {
        return model.getName();
    }    
    public String getPersonPhone() {
        return model.getPhone();
    }    
    public String getPersonAddress() {
        return model.getAddress();
    }    
    public String getPersonBirthDay() {
        return model.getBirthDay();
    }    
    public String getPersonNationality() {
        return model.getNationality();
    }    
    public String getPersonLanguage() {
        return model.getLanguage();
    }    
    public String getPersonSocialSecurityNumber() {
        return model.getSocialSecurityNumber();
    }
    
    //setters: Person
    public void setPersonName(String name) {
        model.setName(name);
    }    
    public void setPersonPhone(String phone) {
        model.setPhone(phone);
    }    
    public void setPersonAddress(String addr) {
        model.setAddress(addr);
    }    
    public void setPersonBirthDay(String bDay) {
        model.setBirthDay(bDay);
    }    
    public void setPersonNationality(String nat) {
        model.setNationality(nat);
    }    
    public void setPersonLanguage(String lan) {
        model.setLanguage(lan);
    }    
    public void setPersonSocialSecurityNumber(String soc) {
        model.setSocialSecurityNumber(soc);
    }   
    
    //getters: User
    public String getUserUsername() {
        return user.getUsername();
    }
    public String getUserPassword() {
        return user.getPassword();
    }
    public String getDatabaseUsername() {
        return user.getUsernameDB();
    }
    public String getDatabasePassword() {
        return user.getPasswordDB();
    }
    public boolean accessGranted() {
        return (user.getUsername().equals(user.getUsernameDB()) && user.getPassword().equals(user.getPasswordDB()));
    }
    
    public void updateView() {
        view.printInformation(model.getId(), model.getName(), model.getPhone(), model.getAddress(), model.getBirthDay(), model.getNationality(), model.getLanguage(), model.getSocialSecurityNumber());
    }
    
    public void updatePersonInformation(String fieldToUpdate, String newValue) {
        fieldToUpdate = fieldToUpdate.toLowerCase();
        switch(fieldToUpdate) {
            case "name" : 
                System.out.println("Changing field " + fieldToUpdate + " from " + model.getName() + " to " +  newValue);
                model.setName(newValue);
                break;
            case "phone" : 
                System.out.println("Changing field " + fieldToUpdate + " from " + model.getPhone() + " to " +  newValue);
                model.setPhone(newValue);
                break;
            case "address" : 
                System.out.println("Changing field " + fieldToUpdate + " from " + model.getAddress() + " to " +  newValue);
                model.setAddress(newValue);
                break;
            case "birthday" : 
                System.out.println("Changing field " + fieldToUpdate + " from " + model.getBirthDay() + " to " +  newValue);
                model.setBirthDay(newValue);
                break;
            case "nationality" : 
                System.out.println("Changing field " + fieldToUpdate + " from " + model.getNationality() + " to " +  newValue);
                model.setNationality(newValue);
                break;
            case "language" : 
                System.out.println("Changing field " + fieldToUpdate + " from " + model.getLanguage() + " to " +  newValue);
                model.setLanguage(newValue);
                break;
            case "social security number" : 
                System.out.println("Changing field " + fieldToUpdate + " from " + model.getSocialSecurityNumber() + " to " +  newValue);
                model.setSocialSecurityNumber(newValue);
                break;
            default: 
                
        }
    }
}
