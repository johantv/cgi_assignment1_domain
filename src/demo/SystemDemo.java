/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

import controller.SystemController;
import java.util.ArrayList;
import model.Person;
import view.SystemView;

/**
 *
 * @author johantv
 */
public class SystemDemo {
    public static void main(String[] args) {
        //create new persons
        Person p = new Person(1, "Johanna Virtanen", "0469455310", "Vellamonkatu 25, Helsinki", "07.09.1992", "suomalainen", "suomi", "070992-xxxx");
        Person p2 = new Person(2, "Matti Meikäläinen", "0101234567", "Matinmäki 1, Mattila", "01.12.2000", "suomalainen", "ruotsi", "011200-yyyy");
        
        //create view
        SystemView v = new SystemView();
        
        //create controller
        SystemController c = new SystemController(p, v);
        c.updateView();
        c.updatePersonInformation("name", "Johanna Heikkinen");
        
        SystemController c2 = new SystemController(p2, v);
        c2.updateView();
        c2.updatePersonInformation("address", "Meikäläisenmäki 1, Meikämaa");
        
        //update view
        c.updateView();
        c2.updateView();
       

        
    }
}
