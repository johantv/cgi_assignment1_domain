/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author johantv
 */
public class UserTest {
    
    User u = new User("exampleun", "examplepw");

    /**
     * Test of getUsername method, of class User.
     */
    @Test
    public void testGetUsername() {
        System.out.println("getUsername (from GUI)");
        assertEquals("getting username from GUI not working", "exampleun", u.getUsername());
    }

    /**
     * Test of getPassword method, of class User.
     */
    @Test
    public void testGetPassword() {
        System.out.println("getPassword (from GUI)");
        assertEquals("getting password from GUI not working", "examplepw", u.getPassword());
    }
    
}
