/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author johantv
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({model.LoggingFailTest.class, model.PersonTestModifying.class, model.PersonTestAdding.class, model.UserTest.class, model.LoggingTest.class})
public class SystemTestSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println("*** STARTING TO RUN TESTS ***");
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        System.out.println("*** FINISHED RUNNING TESTS ***");
    }
    
}
