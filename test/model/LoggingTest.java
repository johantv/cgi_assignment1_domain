/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.SystemController;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import view.SystemView;

/**
 *
 * @author johantv
 */
public class LoggingTest {
    
    public LoggingTest() {
    }
    
    //create new person
    User u = new User("exampleun", "examplepw");
    //create view
    SystemView v = new SystemView();
    //create controller
    SystemController c = new SystemController(u, v);

    /**
     * Test of getUsernameDB method, of class User.
     */
    @Test
    public void testGetUsernameDB() {
        System.out.println("getUsernameDB");
        assertEquals("Getting username from database not working", "exampleun", u.getUsername());
    }

    /**
     * Test of getPasswordDB method, of class User.
     */
    @Test
    public void testGetPasswordDB() {
        System.out.println("getPasswordDB");
        assertEquals("Getting password from database not working", "examplepw", u.getPassword());
    }
    /**
     * Test of grantAccess method
     */
    @Test
    public void testAccessGranting() {
        System.out.println("testing logging in");
        assertEquals("Logging in not working", true, c.accessGranted());
    }
}
