/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.SystemController;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import view.SystemView;

/**
 *
 * @author johantv
 */
public class LoggingFailTest {
    
    public LoggingFailTest() {
    }
    
    //create new person
    User u = new User("exampleun", "wrong");
    //create view
    SystemView v = new SystemView();
    //create controller
    SystemController c = new SystemController(u, v);

    /**
     * Test of grantAccess method
     */
    @Test
    public void testAccessGranting() {
        System.out.println("testing logging in");
        assertEquals("Logging is working with wrong password", false, c.accessGranted());
    }
}
