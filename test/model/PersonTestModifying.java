/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.SystemController;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import view.SystemView;

/**
 *
 * @author johantv
 */
public class PersonTestModifying {
    
    public PersonTestModifying() {
    }
    
    //create new person
    Person p = new Person(1, "Johanna Virtanen", "0469455310", "Vellamonkatu 25, Helsinki", "07.09.1992", "suomalainen", "suomi", "070992-xxxx");
    //create view
    SystemView v = new SystemView();
    //create controller
    SystemController c = new SystemController(p, v);
    
    

    /**
     * Test of modifyPerson method, of class Person.
     */
    @Test
    public void testModifyAddress() {
        c.updatePersonInformation("address", "Uusiosoite 1, Espoo");
        String expected = "Uusiosoite 1, Espoo";
        assertEquals("Modifying address not working", expected, c.getPersonAddress());
    }
    
    @Test
    public void testModifyName() {
        c.updatePersonInformation("NAME", "Johanna Tuulia Virtanen");
        String expected = "Johanna Tuulia Virtanen";
        assertEquals("Modifying name not working", expected, c.getPersonName());
    }
    
    @Test
    public void testModifyLanguage() {
        c.updatePersonInformation("Language", "svenska");
        String expected = "svenska";
        assertEquals("Modifying language not working", expected, c.getPersonLanguage());
    }
}
