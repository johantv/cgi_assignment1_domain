/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.SystemController;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import view.SystemView;

/**
 *
 * @author johantv
 */
public class PersonTestAdding {
    
    public PersonTestAdding() {
    }
    
    //create new person
    Person p = new Person(1, "Johanna Virtanen", "0469455310", "Vellamonkatu 25, Helsinki", "07.09.1992", "suomalainen", "suomi", "070992-xxxx");
    //create view
    SystemView v = new SystemView();
    //create controller
    SystemController c = new SystemController(p, v);

    /**
     * Test of getId method, of class Person.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        assertEquals("Id is not correct", 1, c.getPersonId());
    }

    /**
     * Test of getName method, of class Person.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        assertEquals("Name is not correct", "Johanna Virtanen", c.getPersonName());
        
    }

    /**
     * Test of getPhone method, of class Person.
     */
    @Test
    public void testGetPhone() {
        System.out.println("getPhone");
        assertEquals("Phone number is not correct", "0469455310", c.getPersonPhone());
        
    }

    /**
     * Test of getAddress method, of class Person.
     */
    @Test
    public void testGetAddress() {
        System.out.println("getAddress");
        assertEquals("Address is not correct", "Vellamonkatu 25, Helsinki", c.getPersonAddress());
        
    }

    /**
     * Test of getBirthDay method, of class Person.
     */
    @Test
    public void testGetBirthDay() {
        System.out.println("getBirthDay");
        assertEquals("Birthday is not correct", "07.09.1992", c.getPersonBirthDay());
        
    }

    /**
     * Test of getNationality method, of class Person.
     */
    @Test
    public void testGetNationality() {
        System.out.println("getNationality");
        assertEquals("Nationality is not correct", "suomalainen", c.getPersonNationality());
        
    }

    /**
     * Test of getLanguage method, of class Person.
     */
    @Test
    public void testGetLanguage() {
        System.out.println("getLanguage");
        assertEquals("Language is not correct", "suomi", c.getPersonLanguage());
        
    }

    /**
     * Test of getSocialSecurityNumber method, of class Person.
     */
    @Test
    public void testGetSocialSecurityNumber() {
        System.out.println("getSocialSecurityNumber");
        assertEquals("Social security number is not correct", "070992-xxxx", c.getPersonSocialSecurityNumber());
        
    }
}
